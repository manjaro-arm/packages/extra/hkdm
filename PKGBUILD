# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Adam Thiede <me@adamthiede.com>
# Contributor: Caleb Connolly <caleb@connolly.tech>
# https://git.alpinelinux.org/aports/tree/community/hkdm/APKBUILD

pkgname=hkdm
pkgver=0.2.1
pkgrel=2
pkgdesc="Lighter-weight hotkey daemon"
arch=('x86_64' 'aarch64')
url="https://gitlab.com/postmarketOS/hkdm"
license=('GPL3')
depends=('gcc-libs' 'libevdev')
makedepends=('cargo')
source=("https://gitlab.com/postmarketOS/hkdm/-/archive/$pkgver/$pkgname-$pkgver.tar.gz"
        "$pkgname.service")
sha256sums=('0ae19ee83be1d843958dd2bcba07c17359abdf1e63f1862ca9ee8e979bafd9eb'
            'a2e3fa1340e485b3b73c071b7dab7e60b74cf7f50533f22526b8f29133645419')

prepare() {
  cd "$pkgname-$pkgver"
  export RUSTUP_TOOLCHAIN=stable
  cargo fetch --locked --target "$(rustc -vV | sed -n 's/host: //p')"
}

build() {
  cd "$pkgname-$pkgver"
  export RUSTUP_TOOLCHAIN=stable
  export CARGO_TARGET_DIR=target
  cargo build --frozen --release --all-features
}

check() {
  cd "$pkgname-$pkgver"
  export RUSTUP_TOOLCHAIN=stable
  cargo test --frozen --all-features
}

package() {
  cd "$pkgname-$pkgver"
  install -Dm755 "target/release/$pkgname" -t "$pkgdir/usr/bin/"
  install -Dm644 "$pkgname.example.toml" \
    "$pkgdir/etc/$pkgname/config.d/$pkgname.toml.example"
  install -Dm644 "$srcdir/$pkgname.service" -t "$pkgdir/usr/lib/systemd/system/"
}
